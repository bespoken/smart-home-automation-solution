# **Bespoken Smart Home Testing - Comprehensive Automation Solution**
This sample project is a boilerplate to start doing functional testing for smart home devices like lamps, plugs, etc. It leverages Bespoken, DataDog, and GitLab CI.

The combination of these tools provides in-depth testing, routine builds, tracking of historical results, and graphical reporting and analytics.

It's a powerful combination!

## **Getting Started**
### **Getting Started With GitLab**
We recommend running tests inside of GitLab. It makes it easy to:
* Run tests in a clean environment
* Run tests on a regular schedule
* Share your tests with other team members

To get started using these tests in GitLab, you can just select 'Fork' on the top-right and follow the directions.  
(If you have not already signed up for a GitLab account, you will need to do this first)

It will duplicate this project, in your own GitLab environment!

Once forked, you can drop-in your own tests in the `input` folder to begin using them to test your own voice experiences.

### **Getting Started Locally**
Alternatively, you can run the tests within your own machine (or in a different hosted environment).

Get your local copy of this project by cloning it from this repository.
```bash
git clone https://gitlab.com/bespoken/smart-home-automation-solution.git
```

To make it work, you have to install the project dependencies from the project root:
```bash
npm install
```

### **Virtual Device Setup**
We use Bespoken Virtual Devices to transform the text utterances into audio, send them to the voice assistant and process the responses. A virtual device works like a physical device, such as Amazon Echo, but can be interacted with via our [simple test scripting syntax](https://read.bespoken.io/end-to-end/guide.html) (as well as programmatically via our [API](https://read.bespoken.io/end-to-end/api.html)).

- Create a virtual device with our easy-to-follow guide [here](https://read.bespoken.io/end-to-end/setup/#creating-a-virtual-device).
- Add it to the configuration file ([see below](#project-configuration) for more information):
  ```json
  {  
   "virtualDevices": {
     "<YOUR_VIRTUAL_DEVICE>": ["metadata for your virtual device"]
    }
  }
  ```
### **Environment Variables**
The `DATADOG_API_KEY` environment variable is used during test execution to send the results to DataDog for reporting. It is necessary to create a DataDog account and then generate an API key to receive the test data.

To add these in GitLab, go to `Settings -> CI / CD -> Variables` and add the environment variables, with the appropriatve values, there.

For running locally, both values can be set either in a `.env` file (you can rename the `example.env` and include your values on it); or set them temporarily from the command line with the `export` command:
```bash
export DATADOG_API_KEY=<Your DataDog API Key>
```
(Use `set` instead of `export` if using Windows Command Prompt).

### **Project Configuration**
This sample project can be used to test single or multiple device types. So for example, if you want to check how all your lamps and bulbs are working you can start with the `input/singleTypeDevices.csv` and the `input/singleDeviceTypeUtterances.csv` files. 

The first file contains all the devices you want to test. The second file contains all the utterances you want to execute agains each device from `singleTypeDevices.csv`.

For example, this is how the `input/singleTypeDevices.csv` file looks like:
```
name
office bulb
garden lamp
bedroom sconce
```
And this is how `input/singleDeviceTypeUtterances.csv` file looks like:
```
utterance,transcript
turn ${device} on,*
set brightness on ${device} to 30%,*
increase brightness on ${device} by 50%,*
dim ${device},*
brighten ${device},*
${device} off,*
```
When executing the tests, each utterance will be executed for all the defined devices in `singleTypeDevices.csv`.

To test multiple device types just use one file, like `input/multipleDeviceTypesUtterances.csv`:
```
utterance,transcript
show me james bond movies on office roku,getting james bond from roku
turn office bulb on,okay
set brightness on office bulb to 30%,*
turn office plug on,*
```
Notice we also have two configuration files, one per each case:
* For single device types: `input/singleDeviceType.json`
* For multiple device types: `input/multipleDeviceTypes.json`

A configuration file looks like this:
```json
{
  "customer": "bespoken",
  "job": "smart-home-device-tester",
  "type": "multiple",
  "sourceFile": "./input/multipleDeviceTypesUtterances.csv",
  "interceptor": "./src/smart-home-interceptor",
  "metrics": "datadog-metrics",
  "virtualDevices": {
    "<YOUR_VIRTUAL_DEVICE>": ["metadata for your virtual device"]
  }
}
```
* **`customer`, `job`, `metrics`**: We use DataDog to show the test results. The `customer` and `job` values will be shown in DataDog dashboard.
* **`sourceFile`**: The code to generate the utterances when testing single device types.
* **`interceptor`**: The code where you can get the test results make extra processing and send the information to DataDog.
* **`type`**: The type of testing we are going to perform. Possible values are `single` and `multiple`.

## **Running Tests**
### **Running Tests In GitLab**
To manually start a test run in GitLab, do this: 
* Go to `CI / CD -> Pipelines`
* Click `Run Pipeline`
* Click `Run Pipeline` again

Easy, right?

If using this project as a Fork from GitLab, the testing are controlled via the file `.GitLab-ci.yml` (note in this example we are executing the "multiple device types" case):
```yaml
image: node:10

cache:
  paths:
  - node_modules/

utterance-tests:
  script:
   - npm install
   - npm run processMultiple 
  artifacts:
    when: always
    paths:
      - output/results.csv
  only: 
    - schedules
    - web
```

When the GitLab Runner is executed, it takes this file and creates a Linux instance with Node.js, executes the commands under the `script` element, and saves the reports as artifacts.

#### **Setting a schedule**
It is very easy to run your end-to-end tests regularly using GitLab. Once your CI file (`.GitLab-ci.yml`) has been uploaded to the repository just go to "CI/CD => Schedules" from the left menu. Create a new schedule, it looks like this:

[<img src="docs/images/GitLabCISchedule-1.png" width="50%">](docs/images/GitLabCISchedule-1.png)

[<img src="docs/images/GitLabCISchedule-2.png" width="50%">](docs/images/GitLabCISchedule-2.png)

### **Running Tests Locally**
Once installed the dependencies and added your Virtual Device to the appropriate configuration file, execute the tests with:
* For single device types: `npm run processSingle`
* For multiple device types: `npm run processMultiple`

A report (`results.csv`) is generated under the `output` folder.

[<img src="docs/images/TestReport.png" width="50%">](docs/images/TestReport.png)

## **Test Results**
### **GitLab Test Results**
You can also see the test results if the execution was completed on GitLab CI. To check the output just open the test tab of any completed pipeline:

[<img src="docs/images/GitLabCITestResults-1.png" width="50%">](GitLabCITestResults-1.png)

Then, click on any of the executed test suites:

[<img src="docs/images/GitLabCITestResults-2.png" width="50%">](GitLabCITestResults-2.png)

All the executed tests, and their results will be displayed.

## **DataDog**
DataDog captures metrics related to how all the tests have performed. Each time we run the tests, we push the result of each test to DataDog.

We are using these metrics:
- `utterance.success`
- `utterance.failure`

The metrics can be easily reported on through a DataDog Dashboard. They also can be used to set up notifications when certain conditions are triggered.

### **How to signup and get API key**
[DataDog](https://www.datadoghq.com/) has several plans for different use cases. You can start a 14-day free trial and get your API Key to collect the test results on your dashboard, or use ours, included in our packaged solutions (for more information, [contact us](mailto:sales@bespoken.io)).

After the signup process, you can create your API key following these steps:
1. Select `APIs` from the left `Integrations` menu.
2. Create a new API key in the `API Keys` section.

Then you can copy the value to your `.env` file, or add it as an environment variable on your CI settings.

### **How to export and import a Dashboard**
A DataDog dashboard is a tool for visually tracking, analyzing, and displaying key performance metrics, which enable you to monitor the health of your voice apps. You can create a Dashboard by using the `utterance` and `test` metrics. To help you get started you can import our sample Dashboard by following the next steps:
1. Create a dashboard by selecting `New Dashboard` from the left `Dashboards` menu.
2. Give your dashboard a name, and then click on the `New Timeboard`.
3. Select `Import Dashboard JSON` from the settings cog (upper right corner)
4. Drag [this file](https://gitlab.com/bespoken/smart-home-automation-solution/-/blob/master/docs/BespokenDataDogDashboard.json) into the `Import dashboard JSON` modal window
5. Click on `Yes, Replace`

Now, whenever you run your test scripts, the test results will be automatically sent and stored in DataDog.

[<img src="docs/images/DataDogDashboard.png" width="50%">](docs/images/DataDogDashboard.png)