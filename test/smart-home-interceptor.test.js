/* eslint-env jest */

const { Config, Record, Result } = require('bespoken-batch-tester')
const SmartHomeInterceptor = require('../src/smart-home-interceptor')

require('dotenv').config()

describe('testing results', () => {
  beforeEach(() => {
    Config.reset()
  })

  test('record that is valid - Single Device Type', async () => {
    Config.loadFromJSON({
      customer: 'bespoken',
      job: 'test',
      type: 'single'
    })

    const interceptor = new SmartHomeInterceptor()
    const record = new Record('turn on lamp',
      { state: 'ON', brightness: 80 },
      {
        device: { state: 'ON', brightness: 80 },
        utterance: { utterance: '${device} utterance' }
      })
    const result = new Result(record, 'Joey', {
      transcript: 'okay'
    })
    result.success = true
    await interceptor.interceptResult(record, result)
    expect(result.success).toBe(true)
  })

  test('record that is valid - Multiple Device Type', async () => {
    Config.loadFromJSON({
      customer: 'bespoken',
      job: 'test',
      type: 'multiple'
    })

    const interceptor = new SmartHomeInterceptor()
    const record = new Record('turn on lamp',
      { state: 'ON', brightness: 80 },
      {
        device: { state: 'ON', brightness: 80 },
        utterance: { utterance: '${device} utterance' }
      })
    const result = new Result(record, 'Joey', {
      transcript: 'okay'
    })
    result.success = true
    await interceptor.interceptResult(record, result)
    expect(result.success).toBe(true)
  })

})
