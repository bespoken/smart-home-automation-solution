const { Interceptor, Record, Result, Source, Config } = require('bespoken-batch-tester')

class SmartHomeInterceptor extends Interceptor {
  /**
   *
   * @param {Record} record
   * @param {Result} result
   */
  async interceptResult (record, result) {
    const testType = Config.get('type', [], true, '')
    let template

    // Add a tag for the raw utterance used to create this
    if (testType === 'single') {
      template = record.meta.utterance.utterance.split('${device}').join('device') // eslint-disable-line
    } else if (testType === 'multiple') {
      template = record.utterance
    } else {
      console.error('CONFIG FATAL ERROR: type is required in configuration but is not set or has an invalid values. Exiting.')
      process.exit(1)
    }
    
    result.addTag('utterance', template)

    // These are all valid acknowledgements from Alexa and Google
    const goodResponses = ['okay', 'ok', 'sure', 'shore', 'getting']
    result.addOutputField('transcript', result.lastResponse.transcript)
    if (!result.lastResponse.transcript) {
      result.success = false
    } else {
      result.success = goodResponses.some(goodResponse => result.lastResponse.transcript.toLowerCase().includes(goodResponse))
    }
  }
}

module.exports = SmartHomeInterceptor
